SELECT *
		FROM city_list WHERE country = 'India';
        
SELECT *
		FROM city_data
	LIMIT 10;
    
SELECT *
		FROM global_data
	LIMIT 10;

SELECT *
		FROM global_data
        WHERE year BETWEEN '1800' AND '2013';
        
SELECT *
		FROM city_data
	WHERE city = 'Pune' AND
	(year BETWEEN '1849' AND '2013');
    
SELECT *
		FROM global_data
	WHERE year BETWEEN '1849' AND '2013';
    
SELECT *
		FROM city_data
	WHERE city = 'Pune'
    AND (year BETWEEN '1849' AND '2013')
    AND avg_temp = Null;
