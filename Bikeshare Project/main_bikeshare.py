import time
from collections import OrderedDict
import pandas as pd
import numpy as np


## Defining some global variables
## CITY_DATA is used to map the city name to the equivalent .csv file
## MAP_DAY is used to map the short-name of day filter to it's respective full-name
CITY_DATA = { 'chicago': 'chicago.csv',
              'new york city': 'new_york_city.csv',
              'washington': 'washington.csv' }

MAP_DAY = {'mon': 'Monday', 'tue': 'Tuesday', 'wed': 'Wednesday', 'thu': 'Thursday', 'fri': 'Friday', 'sat': 'Saturday', 'sun': 'Sunday'}

MAP_MONTH = {1:'January', 2:'February', 3:'March', 4:'April', 5:'May', 6:'June', 7:'July', 8:'August', 9:'September', 10:'October', 11:'November', 12:'December'}



def get_filters():
    """
    Asks user to specify a city, month, and day to analyze.

    Returns:
        (str) city - name of the city to analyze
        (str) month - name of the month to filter by, or "all" to apply no month filter
        (str) day - name of the day of week to filter by, or "all" to apply no day filter
    """
    print("\n\n")
    print('-'*40)
    print('Hello! Let\'s explore some US bikeshare data!')
    # TO DO: get user input for city (chicago, new york city, washington). HINT: Use a while loop to handle invalid inputs
    while True:
        city = input('Please enter a city name: ').lower().strip()
        try:
            if CITY_DATA[city]: break
        except Exception:
            print('Please enter a valid city name (Chicago/New York City/Washington)\n')


    ## Get user input for month (all, january, february, ... , june)
    ## Used the strip method to clean the input
    ## Handling invalid entries by defaulting to all
    month = input('\nWhich month\'s data do you want to see? (eg: all/jan/feb): ').lower().strip()
    if month == "":
        print('** Invalid month entered, defaulting to all. **\n')
        month = 'all'


    ## Get user input for day of week (all, monday, tuesday, ... sunday).
    ## Used the strip method to clean the input
    ## Handling invalid entries by defaulting to all
    day = input('Which day\'s data do you want to see? (eg: all/mon/tue): ').lower().strip()
    if day == "":
        print('** Invalid day entered, defaulting to all. **\n')
        day = 'all'

    print('-'*40)
    return city, month, day


def load_data(city, month, day):
    """
    Loads data for the specified city and filters by month and day if applicable.

    Args:
        (str) city - name of the city to analyze
        (str) month - name of the month to filter by, or "all" to apply no month filter
        (str) day - name of the day of week to filter by, or "all" to apply no day filter
    Returns:
        df - Pandas DataFrame containing city data filtered by month and day
    """

    df = pd.read_csv(CITY_DATA[city])

    df['Start Time'] = pd.to_datetime(df['Start Time'])

    df['month'] = df['Start Time'].dt.month
    df['day_of_week'] = df['Start Time'].dt.weekday_name
    
    backup = df.copy()

    ## Handling invalid entries by defaulting to all
    if month!='all':
        ## Making the program scalable to new month entries
        months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
        try:
            month = months.index(month) + 1
            df = df[df['month'] == month]
            ## Incase of no data after applying the filter, reverting back to the original dataframe
            if len(df.index) < 1:
                print('No data for this month. Defaulting to month = all filter.')
                df = backup.copy()
        except (KeyError, ValueError):
            print('** Invalid month, defaulting to all. **')

    ## Handling invalid entries by defaulting to all
    if day!='all':
        try:
            df = df[df['day_of_week'] == MAP_DAY[day]]
        except KeyError:
            print('** Invalid day, defaulting to all. **')

    ## Linearly interpolating the dataframe before processing it
    df.interpolate(method='linear', axis=0, inplace=True)

    ## Dropping the rows with NaN values
    df.dropna(axis=0, inplace=True)

    ## Returning the filtered and pre-processed dataframe
    return df


def time_stats(df):
    """INPUT: Dataframe
    OUTPUT: Statistics on the most frequent month, day and hour of travel
    """

    print('\n\nCalculating The Most Frequent Times of Travel...\n')
    start_time = time.time()

    ## Display the most common month, day of the week and start hour
    ## Using the mode() method for finding the most occurring values in each field
    most_common_month = df['month'].mode()[0]
    most_common_day = df['day_of_week'].mode()[0]
    most_common_st_hour = df['Start Time'].dt.hour.mode()[0]


    print('The most common month is ' + MAP_MONTH[most_common_month])
    print('The most common day is ' + most_common_day)

    ## Converting the 24 HR format to 12 HR format
    if most_common_st_hour > 12:
        stat = 'PM'
    else: stat = 'AM'

    print('The most common start hour is {0} ({1} {2})'.format(most_common_st_hour, most_common_st_hour%12, stat))

    print("\nThis took %s seconds." % (time.time() - start_time))
    print('-'*40)


def station_stats(df):
    """INPUT: Dataframe
    OUTPUT: Statistics on the most popular stations."""

    print('\nCalculating The Most Popular Stations and Trip...\n')
    start_time = time.time()

    ## Display most commonly used start station and end station, respectively
    start_station = df['Start Station'].value_counts().argmax()
    end_station = df['End Station'].value_counts().argmax()
    
    print("The most common start station and end station are ** {0} ** and ** {1} **, respectively.".format(start_station, end_station))
    
    ## Display most frequent combination of start station and end station trip
    ## The answer, after sorting, is returned as a tuple of values. 
    freq_start_station = df.groupby(['Start Station', 'End Station']).size().sort_values(ascending=False).argmax()[0]
    freq_end_station = df.groupby(['Start Station', 'End Station']).size().sort_values(ascending=False).argmax()[1]
    print("The most frequent combination of start station and end station is ** {0} - {1} **".format(freq_start_station, freq_end_station))

    print("\nThis took %s seconds." % (time.time() - start_time))
    print('-'*40)


def trip_duration_stats(df):
    """INPUT: Dataframe
    OUTPUT: Statistics on the total and average trip duration."""

    print('\n\nCalculating Trip Duration...\n')
    start_time = time.time()

    ## Display total and mean travel time
    ## Rounded off the mean time to 2 decimal points
    total_time = df['Trip Duration'].sum()
    mean_time = df['Trip Duration'].mean().round(2)
    print("Total travel time is {0}\nMean Travel time is ~{1}".format(total_time, mean_time))

    print("\nThis took %s seconds." % (time.time() - start_time))
    print('-'*40)


def user_stats(df):
    """INPUT: Dataframe
    OUTPUT: Statistics the user categories, gender and birth year.
    """

    print('\n\nCalculating User Stats...\n')
    start_time = time.time()

    #Display counts of user types
    print("What are the different types of users?")
    print(df['User Type'].value_counts())

    ## Handling missing columns/data in washington.csv
    try:
        ## Display counts of gender
        print("\n{}\n\n".format(df['Gender'].value_counts()))


        ## Pre-calculating the values so that the program can be read easily
        earliest_year = df.sort_values('Birth Year', ascending = True)['Birth Year'].iloc[0]
        recent_year = df.sort_values('Birth Year', ascending = False)['Birth Year'].iloc[0]
        common_year = df['Birth Year'].mode()[0]

        ## Display earliest, most recent, and most common year of birth
        ## Using index in format to avoid leaks
        print("\nWhat is the most earliest, recent and common year of birth?")
        print('The most earliest, recent and common year of birth are {0}, {1} and {2}, respectively.\n'.format(int(earliest_year), int(recent_year), int(common_year)))

    except KeyError:
        print("\n** Incomplete/errored data. Kindly check your .csv file. **")


    print("\nThis took %s seconds." % (time.time() - start_time))
    print('-'*40)

## Generates one record of raw data at a time
def raw_data(list):
    for item in list:
        yield item


def main():
    ## Running the entire program in this while-loop
    while True:
        city, month, day = get_filters()
        df = load_data(city, month, day)

        time_stats(df)
        station_stats(df)
        trip_duration_stats(df)
        user_stats(df)
       

       ## Displaying the raw data one by one
       ## Converting the dataframe into a list or records
        print('\nPlease wait...')
        df = list(df.to_dict('records'))
        ## Retrieving an element from the list via a generator
        count = 1
        for element in raw_data(df):
            choice = input('\n\nDo you want to see the raw data? (Enter y/n).\n').lower().strip()
            if choice == 'y':
                print("\n\nViewing record [{0}/{1}]:\n".format(count, len(df)))
                ## Displaying the element in a more formatted way
                for key, value in element.items():
                    print("{}: {},".format(key, value))
                count += 1
            else:   break

        restart = input('\nWould you like to restart? (Enter y/n).\n').lower().strip()
        if restart != 'y':
            break


if __name__ == "__main__":
	main()
